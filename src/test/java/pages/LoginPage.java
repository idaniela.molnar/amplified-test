package pages;

import models.AccountModel;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

public class LoginPage extends BasePage {

    @FindBy(xpath = "//input[@type='email']")
    private WebElement loginEmailInputField;

    @FindBy(xpath = "//input[@type='password']")
    private WebElement loginPasswordInputField;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement loginButton;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void login(String email, String password) {
        loginEmailInputField.clear();
        loginEmailInputField.sendKeys(email);

        loginPasswordInputField.clear();
        loginPasswordInputField.sendKeys(password);

        loginButton.click();
    }
}
