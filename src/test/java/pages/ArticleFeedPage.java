package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ArticleFeedPage extends BasePage {

    @FindBy(xpath = "//aux-article-list-item[last()]//pp-dropdown/button")
    private WebElement moreOptionsButtonOfLastArticle;

    @FindBy(xpath = "//pp-dropdown[@class='action open']//p[contains(text(),'Edit')]")
    private WebElement editButtonOfLastArticle;

    @FindBy(xpath = "//pp-modal-header/div[contains(text(),'Edit')]")
    private WebElement editArticleModalHeader;

    @FindBy(xpath = "//button[contains(text(),'Save')]")
    private WebElement editArticleSaveButton;

    @FindBy(xpath = "//pp-modal-body//input[@placeholder='Article title']")
    private WebElement articleTitleInputField;

    @FindBy(xpath = "//pp-modal-body//input[@placeholder='Publication name']")
    private WebElement publicationNameInputField;

    @FindBy(xpath = "//input[@type='date']")
    private WebElement dateInputField;

    @FindBy(xpath = "//aux-article-list-item[last()]//div[@class='title']")
    private WebElement titleOfLastArticle;

    public ArticleFeedPage(WebDriver driver) {
        super(driver);
    }

    public void clickMoreOptionsButtonOfLastArticle(){
        moreOptionsButtonOfLastArticle.click();
    }

    public void clickEditButtonOfLastArticle(){
        editButtonOfLastArticle.click();
    }

    public boolean editArticleSaveButtonIsDisplayed(){
        return editArticleSaveButton.isDisplayed();
    }

    public void clickEditArticleSaveButton(){
        editArticleSaveButton.click();
    }

    public boolean editArticleHeaderIsDisplayed(){
        return editArticleModalHeader.isDisplayed();
    }

    public void editAndSave(String newArticleTitle, String newPublicationName) throws InterruptedException {
        articleTitleInputField.clear();
        articleTitleInputField.sendKeys(newArticleTitle);
        Thread.sleep(5000);
        publicationNameInputField.clear();
        publicationNameInputField.sendKeys(newPublicationName);
        Thread.sleep(5000);
        clickEditArticleSaveButton();
        Thread.sleep(5000);
    }

    public String getTitleOfLastArticle(){
        return titleOfLastArticle.getText();
    }
}
