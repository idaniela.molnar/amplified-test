package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class NewsFeedPage extends BasePage{

    @FindBy(xpath = "//a[@class='nav__list__item'][contains(@href,'#/dashboard/news-feed')]")
    private WebElement newsFeedMenuLink;

    @FindBy(xpath = "//button[contains(text(), 'Open Search Settings')]")
    private WebElement openSearchSettingsButton;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement searchButton;

    @FindBy(xpath = "//div[@class='option']/label[contains(text(), 'This exact phrase')]")
    private WebElement exactPhraseSearchOption;

    @FindBy(xpath = "//div[@class='option']/label[contains(text(), 'This exact phrase')]/following-sibling::input")
    private WebElement exactPhraseSearchOptionInputField;

    @FindBy(xpath = "//pp-smart-search-status/p[contains(text(),'Prepd found')]")
    private WebElement searchResultsMessage;

    public NewsFeedPage(WebDriver driver) {
        super(driver);
    }

    public boolean newsFeedMenuLinkIsDisplayed(){
        return newsFeedMenuLink.isDisplayed();
    }

    public void clickNewsFeedMenuLink(){
        newsFeedMenuLink.click();
    }

    public void clickOpenSearchSettingsButton(){
        openSearchSettingsButton.click();
    }

    public boolean searchButtonIsDisplayed(){
        return searchButton.isDisplayed();
    }

    public void clickSearchButton(){
        searchButton.click();
    }

    public boolean exactPhraseSearchOptionIsDisplayed(){
        return exactPhraseSearchOption.isDisplayed();
    }

    public void sendExactTextInput(String input){
        exactPhraseSearchOptionInputField.clear();
        exactPhraseSearchOptionInputField.sendKeys(input);
    }

    public boolean searchResultsMessageIsDisplayed(){
        return searchResultsMessage.isDisplayed();
    }
}
