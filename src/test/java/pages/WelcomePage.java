package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class WelcomePage {

    @FindBy(xpath = "//img[@title='Toggle User Menu']")
    private WebElement userProfileIcon;

    public WelcomePage(WebDriver driver) {
        super();
    }

    public boolean userProfileIconIsDisplayed() {
        return userProfileIcon.isDisplayed();
    }

}
