package base;

public enum Browser {
    CHROME,
    FIREFOX,
    SAFARI
}
