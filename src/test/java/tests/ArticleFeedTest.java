package tests;

import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.ArticleFeedPage;
import pages.LoginPage;

public class ArticleFeedTest extends BaseTest {
    @Test
    public void articleFeedTest() throws InterruptedException {

        // Login step
        driver.get(BASE_URL + "login");
        LoginPage loginPage = new LoginPage(driver);

        String email = "coachPrepd@mailinator.com";
        String password = "qwerty";

        loginPage.login(email, password);
        Thread.sleep(5000);

        Assert.assertEquals(driver.getCurrentUrl(),"https://library.prepd.in/#/dashboard/article-feed");

        // If Login is successful, then it searches for the last article, clicks on the "More Options" button
        // then it clicks on "Edit article"
        ArticleFeedPage articleFeedPage = new ArticleFeedPage(driver);

        articleFeedPage.clickMoreOptionsButtonOfLastArticle();
        articleFeedPage.clickEditButtonOfLastArticle();
        Thread.sleep(5000);

        // Assert that the Edit article modal was successfully opened

        Assert.assertTrue(articleFeedPage.editArticleHeaderIsDisplayed() && articleFeedPage.editArticleSaveButtonIsDisplayed());

        // Change article fields and save, then assert the changes were applied
        String newArticleTitle = "DM Test";
        String newPublicationName = "DM";
        articleFeedPage.editAndSave(newArticleTitle, newPublicationName);

        Assert.assertEquals(newArticleTitle, articleFeedPage.getTitleOfLastArticle());
    }
}
