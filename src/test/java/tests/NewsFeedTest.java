package tests;

import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.NewsFeedPage;

public class NewsFeedTest extends BaseTest {

    @Test
    public void newsFeedTest() throws InterruptedException {

        // Login step
        driver.get(BASE_URL + "login");
        LoginPage loginPage = new LoginPage(driver);

        String email = "coachPrepd@mailinator.com";
        String password = "qwerty";

        loginPage.login(email, password);
        Thread.sleep(5000);

        //If Login is successful, then it clicks on the News Feed link in the menu
        NewsFeedPage newsFeedPage = new NewsFeedPage(driver);
        if (newsFeedPage.newsFeedMenuLinkIsDisplayed()){
            newsFeedPage.clickNewsFeedMenuLink();
            Thread.sleep(5000);
        }
        Assert.assertEquals(driver.getCurrentUrl(),"https://library.prepd.in/#/dashboard/news-feed");

        //Clicks on the OpenSearchSettings button and verified that is expanded
        newsFeedPage.clickOpenSearchSettingsButton();
        Thread.sleep(5000);
        Assert.assertTrue(newsFeedPage.searchButtonIsDisplayed());
        Assert.assertTrue(newsFeedPage.exactPhraseSearchOptionIsDisplayed());

        //If the search options menu expanded, it sends input value and performs search, then verifies that the
        // search results message is displayed
        String exactPhraseInput = "At the end of the day";
        newsFeedPage.sendExactTextInput(exactPhraseInput);
        newsFeedPage.clickSearchButton();
        Thread.sleep(8000);
        Assert.assertTrue(newsFeedPage.searchResultsMessageIsDisplayed());

    }
}
