package tests;

import base.BaseTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.WelcomePage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class LoginTest extends BaseTest {
/*    @DataProvider(name = "loginDataProvider")
    public Iterator<Object[]> loginDataProvider() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Collection<Object[]> dataProvider = new ArrayList<>();

        File[] files = getListOfFilesFromResources("login");
        for (File f : files) {
            LoginModel m = mapper.readValue(f, LoginModel.class);
            dataProvider.add(new Object[]{m});
        }

        return dataProvider.iterator();
    }*/

    @Test
    public void loginTest() throws InterruptedException {
        driver.get(BASE_URL + "login");
        LoginPage loginPage = new LoginPage(driver);

        String email = "coachPrepd@mailinator.com";
        String password = "qwerty";

        loginPage.login(email, password);
        Thread.sleep(5000);

        String actualUrl = "https://library.prepd.in/#/dashboard/article-feed";
        String expectedUrl = driver.getCurrentUrl();

        Assert.assertEquals(expectedUrl, actualUrl);

    }
}
